$(document).ready(function () {
    var output;
    var inputField = $(".calciDisplay")
    // dictionary defined using ASCII
    var keyDict = { "48": "0", "49": "1", "50": "2", "51": "3", "52": "4", "53": "5", "54": "6", "55": "7", "56": "8", "57": "9", "42": "*", "43": "+", "45": "-", "46": ".", "47": "/", "40": "(", "41": ")", "37": "%"
    }
    $(document).on('click', '.calciButton', function () {               //to get all the values to field
        var inputVal = $(this).html()
        inputField.val(inputField.val() + inputVal)
    })
    $(document).on('click', '.calciOperator', function () {             //to get all the operators to field
        var inputOpr = $(this).html()
        inputField.val(inputField.val() + inputOpr);
    })
    $(document).on('click', '#allClear', function () {                  //to empty the display and value stored
        inputField.val('');
    });
    $(document).on('click', '#backspace', function () {                 //to remove one single from last
        inputField.val(inputField.val().substring(0, inputField.val().length - 1));
    });
    $(document).on('click', '#enterButton', function () {               //to get the desired answer
        calculation();                                                  //function called to occur calculations
    })
    $(document).keyup(function (event) {                                
        if (event.keyCode == "08") {
            $("#backspace").trigger('click')
        }
    });
    $(document).keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            calculation()
        }
        else if (keyDict.hasOwnProperty(keycode)) {
            inputField.val(inputField.val() + keyDict[keycode]);
        }
    });
    function calculation() {                                        //to do calculations and remove invalids inputs
        try {
            output = eval(inputField.val())
            inputField.val(output)
        } catch {
            alert("Kindly enter the correct expression")
        }
    }
});

