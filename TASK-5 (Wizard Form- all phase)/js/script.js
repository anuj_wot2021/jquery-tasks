$(document).ready(function () {
    // here is the plugins used to mask and format 
    var t = $('#myTable').DataTable();
    var editRowId;                          
    // plug in to maks ipAddress
    $('#ipAddress').mask('099.099.099.099');
    // this is the plug in to check the Ip address
    $('#ipAddress').ipAddress(); 
    // plug in to mask hours   
    $("#Hours").mask("00"); 
    // plug in for zip-code
    $("#ZipCode").mask("000-000");
     // plug in for date-picker
    $('#datepicker').mask('00/00/0000');
    // this is a pligin to add a calender to the date input
    $(function () {
        $("#datepicker").datepicker();
    });
     // to format the email address
    function validateEmailAddress(input) {
        var regex = /[^\s@]+@[^\s@]+\.[^\s@]+/;
        if (regex.test(input)) {
            return 1;
        } else {
            return -1;
        }
    }
    // this is to format the date 
    function isDate(str) {    
        var parms = str.split(/[\.\-\/]/);
        var yyyy = parseInt(parms[2],10);
        var mm   = parseInt(parms[0],10);
        var dd   = parseInt(parms[1],10);
        var date = new Date(yyyy, mm - 1, dd, 0, 0, 0, 0);
        return mm === (date.getMonth()+1) && dd === date.getDate() && yyyy === date.getFullYear();
    }
    // this is the regex for name
    function forName(input) {
        var nameRegex = /^[a-zA-Z]+$/;
        if (nameRegex.test(input)) {
            return 1;
        } else {
            return -1;
        } 
    }
    // here are all the variables defined globally 
    var firstName, lastName,Hours,ZipCode,ipAddress,eMail,phone,dob,sports,about,checkBox;
    var currentrow = null;
    $("#specialCancel").hide();
    $(".save-1").on('click', function () {
        //this is the first save and next button
        firstName = $("#firstname").val();
        lastName = $("#lastname").val();
        gender = $("input[name='gender']:checked").val();
        Hours = $("#Hours").val();
        ZipCode = $("#ZipCode").val();
        ipAddress = $("#ipAddress").val();
        // this condition will work when the values are enetred null or worong
        if (forName(firstName) === -1 || forName(lastName) === -1 || Hours > 18 || Hours == "" || ZipCode.length <= 6 || ipAddress.length <= 12) {
            $("#myForm").valid();
            return false;
        }
        tonext(); 
    });
    $(".save-2").on("click", function () {                          //this is the second save ansd next button
        eMail = $("#email").val();
        phone = $("#phone").val();
        dob = $("#datepicker").val();
        // this condition will work when the values are enetred null or wrong
        if (validateEmailAddress(eMail) === -1 || phone.length <10 || phone.length >10 || isDate(dob) === false) {
            $("#myForm").valid() ;
            return false;
         }
        tonext(); 
    });
    $(".prev-1").on("click", function () {
        $(".step-heading.active").removeClass("active").prev().addClass("active");
        $(".step-content.active").removeClass("active").prev().addClass("active");
    });
    $("#submit").on("click", function () {                           //this is the submit buton
        $("#submit").html("Submit");
        $("#specialCancel").hide();
        sports = $('#sports option:selected').text();
        console.log("sprots" , sports);
        // $("#your_select").val();
        about = $("#about").val();
        var checkBox = ""  
        if($("#agree").prop('checked') != false){
            checkBox = "Agreed"       
        }
        else{
            checkBox = "Dis-agree"
            }
        if (about == "" || checkBox != "Agreed")   {
        $("#myForm").valid(); 
              }
        else{
               toJump();
            var newrow = [firstName, lastName, gender, Hours, ZipCode, ipAddress, eMail, phone, dob, sports, about, checkBox, '<button class="edit">Edit</button>', '<button class="delete">Delete</button>'];
            $("#myForm")[0].reset();  //this will reset the form
        }
        if (editRowId != undefined) {
            t.row(editRowId).data(newrow).draw();   
            editRowId = undefined;
        } else {
            t.row.add(newrow).draw(false);
            }
    });
    // to edit a particular row
    $(document).on("click", ".edit", function () {
        $("#submit").html("UPDATE");
        $("#specialCancel").show();
        editRowId = $(this).closest("tr").index()       //to get the currnet index of row
        currentrow = $(this).closest("tr").children();
        gender = $(this).closest('tr').find('td:eq(2)').text();
        // for first step
        $("#firstname").val($(currentrow[0]).html());
        $("#lastname").val($(currentrow[1]).html());
        $(`#${gender}`).prop("checked", true);
        $("#Hours").val($(currentrow[3]).html());
        $("#ZipCode").val($(currentrow[4]).html());
        $("#ipAddress").val($(currentrow[5]).html());
        //for second step
        $("#email").val($(currentrow[6]).html());
        $("#phone").val($(currentrow[7]).html());
        $("#datepicker").val($(currentrow[8]).html());
        //for third row
        $("#sports").val($(currentrow[9]).html());
        $("#about").val($(currentrow[10]).html());
        $('#agree').prop('checked', true);
        
    });     
    $("#specialCancel").on("click", function () {               //this button will appear after update 
        $("#myForm")[0].reset();
        $("#specialCancel").hide();
        $("#submit").html("Submit");
        $("#specialCancel").hide();
        currentrow = null;
        toJump();
    });
    // to delete the perticular row
    $(document).on('click', '.delete', function () {
        t.row($(this).parents('tr')).remove().draw();
        $("#myForm")[0].reset();
        editRowId = null;
        $("#submit").html("Submit");
    });
    function toJump() {
        $(".step-heading.active ").removeClass("active").prev().siblings().addClass("active").prev().siblings().removeClass("active").first().addClass("active");
        $(".step-content.active ").removeClass("active").prev().siblings().addClass("active").prev().siblings().removeClass("active").first().addClass("active");
    }
    function tonext() {
        $(".step-heading.active ").removeClass("active").next().addClass("active");
        $(".step-content.active").removeClass("active").next().addClass("active");
    }
    // for validations we are now using plugins.
    $("#myForm").validate({
        rules: {
            firstname: {
                required: true,
                minlength : 3
            },
            lastname: "required",
              eMail: {
                required: true,
                eMail: true,
            },
            ZipCode: {
                required: true,
                minlength: 7
            },
            ipAddress: {
                required: true,
                minlength: 12
            },
            phone: {
                required: true,
                maxlength: 10,
                minlength: 10,
            },
            Hours: {
                required: true,
                min: 0,
                max: 18,
                minlength: 2
            },
            datepicker: {
                required: true,
                date: true
            },
            about: "required",
            agree: "required"
        },
        messages: {
            firstname: "Please enter first name",
            lastname: "Please enter last name",
            Hours: "Better go and sleep!",
            ipAddress: "Please enter valid values",
            eMail: "Please enter valid email address",
            ZipCode: "please enter zip code",
            phone: "Please enter your Contact",
            datepicker: "Please enter vaild dob",
            about: "Let us know you!",
            agree: "Please accept the terms"
        },
      
    });
    
});
