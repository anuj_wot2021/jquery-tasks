/*

function show(selected) {
    var allDispyaText = document.getElementsByClassName("description")
    var fa = document.getElementsByClassName("fa")
    var selected = document.getElementById(selected);
    var icon = selected.previousElementSibling
    icon = icon.childNodes[1].firstChild
    if (selected.classList.contains("hide")) {
        for (let i = 0; i < allDispyaText.length; i++) {
            allDispyaText[i].classList.add("hide")
            fa[i].classList.remove("fa-minus")
            fa[i].classList.add("fa-plus")
        }
        selected.classList.remove("hide")
        icon.classList.remove("fa-plus")
        icon.classList.add("fa-minus")
    }
    else {
        selected.classList.add("hide")
        icon.classList.remove("fa-minus")
        icon.classList.add("fa-plus")
    }
}
*/

$(function () {
    $(".tablinks").click(function () {
        if ($(this).next().is(":visible")) {
            $(this).next().hide();
            $(".tablinks .fa").eq($(this).parent().index()).removeClass("fa-minus").addClass("fa-plus")
        } else {
            $(".description").hide();
            $(this).next().show();
            $(".tablinks .fa").eq($(this).parent().index()).removeClass("fa-plus").addClass("fa-minus").closest(".tablinks").parent().siblings().find(".fa-minus").removeClass("fa-minus").addClass("fa-plus")
        }
    });
});
