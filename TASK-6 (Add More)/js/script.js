$(document).ready(function () {
  var parentInput, firstChild, secondChild;
  var parentIndex
  var childBox
  var rowLength
  // this function will add the parent box after clicking on the add more button 
  $(document).on("click", "#addMore", function () {
    $("#content").append(
      $("#parentBox").clone().removeClass("hide").removeAttr("id"));
    $(".parentInput").trigger("keyup")

  });
  // here the child box containing all the child inputs will appear after clicking this button
  $(document).on("click", ".addChild", function () {
    $(this).closest(".parentBox").append($("#childBox").clone().removeClass("hide"));
  });
  // here the plugin-bootbox is used to delete the child and its table
  $(document).on("click", ".remove", function () {
    var delRow = $(this);
    bootbox.confirm({
      size: "small",
      message: "Are you sure?",
      callback: function (result) {
        if (result == true) {
          delRow.parent().remove()
          $(".parentInput").trigger("keyup")
        }
      }
    })
  });
  // here the plugin-bootbox is used to delete the parent and table
  $(document).on("click", ".deleteParent", function () {
    var delPar = $(this);
    parentIndex = $(this).parent().index();
    bootbox.confirm({
      size: "small",
      message: "Are you sure?",
      callback: function (result) {
        if (result == true) {
          delPar.parent().remove()
          $(".TableData").eq(parentIndex).remove()
        }
      },
    })
  });
  $(document).on('keyup', '.specialChild', function () {
    var resultstring;
    var updatedData;
    childBox = $(this).closest(".parentBox").children(".childbox");
    var newParentIndex = $(this).closest(".parentBox").index()
    rowLength = childBox.length;
    console.log(newParentIndex);
    var newParentInput = $(this).closest(".parentBox").children(".parentInput").val()
    console.log("newParentInput", newParentInput);

    
    if ($(".TableData").eq(newParentIndex).length > 0) {
      $(".TableData").eq(newParentIndex).html("");
      newParentInput = newParentInput == undefined ? "" : newParentInput
      console.log(newParentInput);
      updatedData += "<tr> <th>" + newParentInput + "</th> </tr>";
      console.log(updatedData);
      for (i = 0; i < rowLength; i++) {
        firstChild = $(childBox).eq(i).find(".first-child").val();
        secondChild = $(childBox).eq(i).find(".second-child").val();
        updatedData += "<tr><td>" + firstChild + "</td>" + "<td>" + secondChild + "</td></tr>";
      }
      console.log("parent is:", newParentIndex);
      $(".TableData").eq(newParentIndex).append(updatedData)
    } else {
      resultstring = `<table class="TableData"> `;
      newParentInput = newParentInput == undefined ? "" : newParentInput
      resultstring += "<tr> <th>" + newParentInput + "</th> </tr>";
      for (i = 0; i < rowLength; i++) {
        firstChild = $(childBox).eq(i).find(".first-child").val();
        secondChild = $(childBox).eq(i).find(".second-child").val();
        resultstring += "<tr><td>" + firstChild + "</td>" + "<td>" + secondChild + "</td></tr>";
      }
      resultstring += "</table>";
      $(".changeoverTable").append(resultstring);
    }
  });

  $(document).on('keyup', '.parentInput', function () {
    parentIndex = $(this).parent().index();
    parentInput = $(this).val()
    childBox = $(this).parent().find("#childBox");
    rowLength = childBox.length;
    var resultstring;
    console.log("if-condition", $(".TableData").eq(parentIndex).length);
    if ($(".TableData").eq(parentIndex).length > 0) {
      $(".TableData").eq(parentIndex).html("");
      var updatedData = `<tr>`
      console.log("here", parentInput);
      updatedData += "<tr> <th>" + parentInput + "</th> </tr>";
      for (i = 0; i < rowLength; i++) {
        firstChild = $(childBox).eq(i).find(".first-child").val();
        secondChild = $(childBox).eq(i).find(".second-child").val();
        updatedData += "<tr><td>" + firstChild + "</td>" + "<td>" + secondChild + "</td></tr>";
      }
      console.log("parent is:", parentIndex);
      $(".TableData").eq(parentIndex).append(updatedData)
      updatedData += `</tr>`
    } else {
      resultstring = `<table class="TableData"> `;
      resultstring += "<tr> <th>" + parentInput + "</th> </tr>";
      for (i = 0; i < rowLength; i++) {
        firstChild = $(childBox).eq(i).find(".first-child").val();
        secondChild = $(childBox).eq(i).find(".second-child").val();
        resultstring += "<tr><td>" + firstChild + "</td>" + "<td>" + secondChild + "</td></tr>";
      }
      resultstring += "</table>";
      $(".changeoverTable").append(resultstring);
    }
  //   $(document).ready(function () {
  //     $('#new-item').click(function() {
  //         console.log($('#text-to-add').val());
  //         $('select').append( '<option>' + $('#text-to-add').val() + '</option>' );
  //     });
  // });
  
  });
});
