$(document).ready(function () {
  var hour = 0;
  var min = 0;
  var sec = 0;                                            //initial values definer
  var realSec = 0;
  var csec = 0;
  var counter = 0;
  $("#Pause").attr("disabled", true);
  $("#Stop").attr("disabled", true);                      //default buttons are kept desiabled
  function main(realSec) {                                //main functions defined for all the calculations
    hour = Math.floor(realSec / 3600);
    min = Math.floor((realSec - hour * 3600) / 60);
    sec = realSec - hour * 3600 - min * 60;
    $("#Hours").text(hour);
    $("#Minutes").text(min);
    $("#Seconds").text(sec);
  }
  function timer() {                                      //timer function defined to start the loop
    $("#CentiSeconds").text(csec);
    csec++;
    if (csec > 99) {
      realSec++;
      main(realSec);
      csec = 0;
    }
  }
  $("#Start").click(function () {                         //click event defined to start the loop
    $("#Pause").attr("disabled", false);
    $("#Stop").attr("disabled", false);
    var status = $("#Start").text();
    if (status == "START") {                              //data-status is initially start
      counter = setInterval(timer, 10);                   //interval set here
      $("#Start").attr("disabled", true);
      $("#Pause").attr("disabled", false);
      $("ol").append("<li> Start at :: Hours:-" + hour + " Minutes:-" + min + " Seconds:-" + sec + " Centi-Seconds:-" + csec + "</li>"
      );
    }
    else if (status == "RESUME") {                        //condition for resume, data-status changed to resume
      $("#Start").attr("disabled", false);
      clearInterval(counter);
      counter = setInterval(timer, 10);
      $("ol").append("<li> Resume at ::  Hours:-" + hour + " Minutes:-" + min + " Seconds:-" + sec + " Centi-Seconds:-" + csec + "</li>");
    }
    else if (status == "RESTART") {                      //condition to restart, data-status changed to restart
      $("#Start").attr("disabled", true);
      realSec = 0;
      counter = setInterval(timer, 10);
      $("ol").append("<li> Restarted at ::  Hours:-" + hour + " Minutes:-" + min + " Seconds:-" + sec + " Centi-Seconds:-" + csec + "</li>");
    }
  });
  $("#Pause").click(function () {                          //condition to pause
    clearTimeout(counter);
    $("#Start").html("RESUME");
    $("#Start").attr("disabled", false);
    $("#Start").attr("data-status", "resume");
    $("ol").append("<li> Pause at :: Hours:-" + hour + " Minutes:-" + min + " Seconds:-" + sec + " Centi-Seconds:-" + csec + "</li>");
  });
  $("#Stop").click(function () {                            //condition to stop
    clearTimeout(counter);
    $("#Start").html("RESTART");
    $("#Start").attr("disabled", false);
    $("#Start").attr("data-status", "restart");
    $("ol").append("<li> Stop at :: Hours:-" + hour + " Minutes:-" + min + " Seconds:-" + sec + " Centi-Seconds:-" + csec + "</li>");
  });
  $("#Reset").click(function () {                         //condition to reset
    $("#Start").html("START");
    $("#Start").attr("disabled", false);
    clearInterval(counter);
    location.reload(true)
  });
});

