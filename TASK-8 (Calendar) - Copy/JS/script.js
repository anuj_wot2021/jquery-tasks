$(document).ready(function () {
// here the variables are defined globally and the values are defined
    let today = new Date()
    var day = today.getDate()
    let currentMonth = today.getMonth()+1
    let currentYear = today.getFullYear()

//here are the arrays defined and the key value passes according to the numbers 
    let monthNames = { 1: "January", 2: "February", 3: "March", 4: "April", 5: "May", 6: "June", 7: "July", 8: "August", 9: "September", 10: "October", 11: "November", 12: "December" };
    var currentMonthandYear = monthNames[today.getMonth() + 1] + " - " + [today.getFullYear()]
// this will append the current name of month and year to the title box
    $("#monthAndYear").html(currentMonthandYear)

// here all the on click functions are defined
    
    // to get the next month 
    $(document).on('click', '#rightArrow', function () {
        currentYear = (currentMonth === 12) ? currentYear + 1 : currentYear;
        currentMonth = (currentMonth+1)
        if (currentMonth == 13) {
            currentMonth = 1;
        }
        $(".tableRow").remove();
        showDates(currentMonth, currentYear)
        $("#monthAndYear").text(monthNames[currentMonth] + " - " + currentYear)
    })
    // to get the prev month 
    $(document).on('click', '.fa-arrow-left ', function () {
        currentYear = (currentMonth === 1) ? currentYear - 1 : currentYear;
        currentMonth = (currentMonth === 1) ? 12 : currentMonth - 1;
        if (currentMonth == 13) {
            currentMonth = 1;
        }
        $(".tableRow").remove();
        showDates(currentMonth, currentYear)
        $("#monthAndYear").text(monthNames[currentMonth] + " - " + currentYear)
    })
    // to get the todays value
    $(document).on('click', "#todayButton", function () {
        location.reload()
    })
    // to find the the entered date in the finder
    $(document).on('click', '#findDate', function () {
        toJump()
        $("#totalDates").html("")
        datesShow()
    })

// here are the on change functions
    
    $('#totalMonths').on('change', function() {
        toJump()
        $("#totalDates").html("")
        datesShow()
      });
    $('#totalYears').on('change', function() {
        toJump()
        $("#totalDates").html("")
        datesShow()
      });

// these are the for loops to set the values of finders in month, year and dates 
    for (i = 1970; i <= 2070; i++){
        $("#totalYears").append($('<option />').val(i).html(i));
    }
    for (i = 1; i <= 31; i++){
        $("#totalDates").append($('<option />').val(i).html(i));
    }

// user defined function to show the values of dates are per entered month  

    function datesShow() {
        enteredMonth = $('#totalMonths option:selected').val();
        enteredYear = $('#totalYears option:selected').val();
        // for months containing the 31 number of dates
        if (enteredMonth == 01 || enteredMonth == 03 || enteredMonth == 05 || enteredMonth == 07 || enteredMonth == 08 || enteredMonth == 10 || enteredMonth == 12) {
            for (i = 1; i <= 31; i++){
                $("#totalDates").append($('<option />').val(i).html(i));
            }
        }
        // for months containing the 30 number of dates
        else if ((enteredMonth == 04 || enteredMonth == 06 || enteredMonth == 09 || enteredMonth == 11)){
            for (i = 1; i <= 30; i++){
                $("#totalDates").append($('<option />').val(i).html(i));
            }
        }
            // for months containing the 29 number of dates
        else if  ((enteredMonth == 02) && ((enteredYear % 4 == 0) && (enteredYear % 100 != 0)) || (enteredYear % 400 == 0) )  {
            for (i = 1; i <= 29; i++){
                $("#totalDates").append($('<option />').val(i).html(i));
            }
        }
            // for months containing the 28 number of dates
        else if (enteredMonth == 02) {
            for (i = 1; i <= 28; i++){
                $("#totalDates").append($('<option />').val(i).html(i));
            }
        }
    }
// this is a user defined function helps to acheive the required month and year entered in the finders
    function toJump() {
        var enteredYear = $('#totalYears option:selected').val();
        var enteredMonth = $('#totalMonths option:selected').val();
        var enterDate = $('#totalDates option:selected').val()
        $(".tableRow").remove();
        showDates(enteredMonth, enteredYear, enterDate)
        $("#monthAndYear").html(monthNames[enteredMonth] +" - "+ enteredYear)
    }

// this is the main user defined function helps to get the current date, first day, number of days in the current running values of month and year

    function showDates(month, year, enterDate = 0) {
        let firstDay = new Date(year, month-1, 1)
        var startingDayNumber = firstDay.getDay()
        var daysInMonth = new Date(year, month, 0).getDate();
        var table = $("#calendar")
        table = "";
        // to append the rows
        for (i = 0; i < 6; i++) {
            let row = "<tr class= 'tableRow'>"
            // to append the columns 
            for (j = 1; j <= 7; j++) {
                if (i === 0 && j <= startingDayNumber) {
                    row += "<td class=tableData>" + " " + "</td>"
                }
                else {
                    date = (j + (7 * i)) - startingDayNumber;
                    // condition to highlight the current dates
                    if (date === day && month == today.getMonth() + 1 && year == today.getFullYear() ){
                        row += "<td class='tableData active'>" + date + "</td>"
                    }
                    // condition to highlight the finder dates
                    else if (enterDate != 0 && date === parseInt(enterDate)) {
                        row += "<td class='tableData enteredDate'>" + date + "</td>"
                    }
                    else if (date <= daysInMonth) {
                        row += "<td class=tableData>" + date + "</td>"
                    }
                }
            }
            // the final row is appended.
            $("#calendar-body").append(row)
        }
    }
    showDates(currentMonth, currentYear)
});