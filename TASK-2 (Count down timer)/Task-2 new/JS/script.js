$(document).ready(function () {
  var hour = 00;
  var min = 00;
  var sec = 00;                               //here all the initial values are defined 
  var csec = 00;
  var currentValue;
  var inputEnt;
  var interval;
  $("#Pause").attr("disabled", true);         //other buttons are disable
  $("#Stop").attr("disabled", true);          
  $("#Start").click(function () {             //the click function defines here
    var status = $("#Start").data("status");
    if (status == "start") {                   //data status changed to start
      inputEnt = $("input").val();             //value is getting input
      inputEnt = parseInt(inputEnt);
      if (inputEnt > 0) {
        $("#Start").attr("disabled", true);
        $("#Pause").attr("disabled", false);
        $("#Stop").attr("disabled", false);
        $("#Reset").attr("disable", false);
        interval = setInterval(function () {    //inter is set here
          timer(inputEnt);
          $("#CentiSeconds").text(csec);
          csec--;
          if (csec <= 0) {
            inputEnt--;
            $("#CentiSeconds").text();
            csec = 99;
          }
        }, 10);
        $("ol").append(
          "<li> Start at :: Hours:-" + hour + " Minutes:-" + min + " Seconds:-" + sec + "</li>"
        );
    } else {
       alert("Plese enter some value");       //alert is used to ask user to enter data other then null 
      }
    } else if (status == "resume") {          //status changed to resume
      clearInterval(interval);
      interval = setInterval(function () {
        timer(inputEnt);
        $("#CentiSeconds").text(csec);
        csec--;
        if (csec <= 0) {
          inputEnt--;
          $("#CentiSeconds").text();
          csec = 99;
        } }, 10);
      $("ol").append(
        "<li> Resume at :: Hours:-" + hour + " Minutes:-" + min + " Seconds:-" + sec + "</li>"
      );
      currentValue = $("inputEnt").text();
      $("#Start").html("<p>START</p>");
      $("#Start").data("status", "start");
      $("#Start").attr("disabled", true);
      }
    else if (status == "restart") {           //status channged to restart
      clearInterval(interval);
      inputEnt = $("input").val();
      interval = setInterval(function () {
        timer(inputEnt);
        $("#CentiSeconds").text(csec);
        csec--;
        if (csec <= 0) {
          inputEnt--;
          $("#CentiSeconds").text();
          csec = 99;
        }
      }, 10);
      $("#Start").html("<p>START</p>");
      $("#Start").data("status", "start");
      $("#Start").attr("disabled", true);
      setTimeout(() => {
         console.log(hour, min, sec);
        $("ol").append(
          "<li> Restart at :: Hours:-" + hour + " Minutes:-" + min + " Seconds:-" + sec + "</li>"
        );
      }, 100);
    }
 });
  $("#Pause").click(function () {             //to pause and to append the values
    clearTimeout(interval);
    $("#Start").html("<p>RESUME</p>");
    $("#Start").attr("disabled", false);
    $("#Start").data("status", "resume");
    $("ol").append(
      "<li> Pause at :: Hours:-" + hour + " Minutes:-" + min + " Seconds:-" + sec + "</li>"
    );
  });
$("#Stop").click(function () {               //to stop and to stop the running values
    clearTimeout(interval);
    $("#Start").html("<p>RESTART</p>");
    $("#Start").attr("disabled", false);
  $("#Start").data("status", "restart");
    $("ol").append(
      "<li> Stop at :: Hours:-" + hour + " Minutes:-" + min + " Seconds:-" + sec + "</li>"
    );
  });
 $("#Reset").click(function () {              //to reset the values and to refresh page
    $("#Start").html("<p>START</p>");
  location.reload()
  });
  function timer(inputEnt) {                  //condition when value is less then zero
    $("#Seconds").text(inputEnt);
    if (inputEnt <= 0) {
      $("#CentiSeconds").text("00");
      clearInterval(interval);
      return;
    }
    if (inputEnt > 1 && inputEnt < 60)        //condition when input is greater then 1 and less then 60
    {
      hour = 00;
      sec = 00;
      min = 00;
      $("#Seconds").text(inputEnt);
      $("#Minutes").text("0");
      sec = $("#Seconds").text();
    }
    else if (inputEnt >= 60 && inputEnt <= 3600)      //condition when input is greater then 60 and less then 3600
    {
      hour = 00;
      $("#Minutes").text(parseInt((Math.floor = (inputEnt % 3600) / 60)));
      min = $("#Minutes").text();
      $("#Seconds").text(parseInt((Math.floor = (inputEnt % 3600) % 60)));
      sec = $("#Seconds").text();
      $("#Hours").text(0);
 }
    else if (inputEnt > 3600)                     //condition when input is greater then 3600
    {
      $("#Hours").text(parseInt((Math.floor = inputEnt / 3600)));
      $("#Minutes").text(parseInt((Math.floor = (inputEnt % 3600) / 60)));
      hour = $("#Hours").text();
      min = $("#Minutes").text();
      $("#Seconds").text(parseInt((Math.floor = (inputEnt % 3600) % 60)));
      sec = $("#Seconds").text();
    }
    else
    {
      $("#CentiSeconds").text("00");
    }
  }
});
